import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TesttheAmazingPeter {
	
	/*
	 * 1. Write a function that says 1 person is amazing.
	 * 2. Modify your function so that when name is null, the function will return: "You is Amazing" 
	 * 3. When name is all uppercase letters, the method should print everything in uppercase
	 * 4. Modify your function to accept 2 people.
	 * 5. Modify the function to handle more than 2 people.
	 * 6. Extend your function to handle a mixture (jumble) of uppercase and regular names.
	*/
	
	//Stub: AmazingPerson()
	//Test Cases For Requirement 1
	@Test
	void testInputIsReturningAnAmazingPerson() {
			String[] arr = {"peter"};
			theAmazingPeter obj = new theAmazingPeter();
			String result = obj.AmazingPerson(arr);
			assertEquals("peter is amazing",result);
		}
	//Test Cases For Requirement 2
	@Test
	void testInputIsNull() {
			theAmazingPeter obj = new theAmazingPeter();
			String result = obj.AmazingPerson(null);
			assertEquals("you is amazing",result);
		}
	//Test Cases For Requirement 3
	@Test
	void testInputIsUpperCase() {
			theAmazingPeter obj = new theAmazingPeter();
			String[] arr = {"PETER"};
			String result = obj.AmazingPerson(arr);
			assertEquals("PETER IS AMAZING",result);
		}
//	Test Cases For Requirement 4
	@Test
	void testAcceptsTwoNumbers() {
			theAmazingPeter obj = new theAmazingPeter();
			String[] arr = {"Peter","Sourav"};
			String result = obj.AmazingPerson(arr);
			assertEquals("Peter and Sourav are amazing",result);
		}
//	Test Cases For Requirement 5
	@Test
	void testAcceptsMoreThanTwoNumbers() {
			theAmazingPeter obj = new theAmazingPeter();
			String[] arr = {"Peter","Jigisha","Naveen","Guneet"};
			String result = obj.AmazingPerson(arr);
			assertEquals("Peter, Jigisha, Naveen, and Guneet are amazing",result);
		}
//	Test Cases For Requirement 6
	@Test
	void testAcceptsAJumbleOfUpperCaseAndLowerCase() {
			theAmazingPeter obj = new theAmazingPeter();
			String[] arr = {"Peter","GUNEET","NAVEEN","JIGISHA"};
			String result = obj.AmazingPerson(arr);
			assertEquals("Peter, GUNEET, NAVEEN, are awesome and JIGISHA also ",result);
		}
	}
